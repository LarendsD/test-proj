export type Person = {
    id: number
    shareholderId: number
    createdAt: Date
    surname: string
    name: string
    patronymic: string | null
    dateOfBirth: Date | null
    placeOfBirth: string | null
    sex: 'M' | 'F' | null
    snils: string | null
    inn: string | null
    withIp: boolean
    ogrnip: string | null
    registrationAddress: number | null
  }