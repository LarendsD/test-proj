import { NewExecutiveDocument } from "../../interfaces/new-executive-document.interface";
import { pgClient } from "../../postgresql";

export const updateCourtCase = async (id: number, data: NewExecutiveDocument['IId']) => {
    const updateData = {
        debt_charges_awarded: data.DebtSum,
        debt_fines_awarded: data.DebtFines,
        debt_fee_awarded: data.DebtFee,
        total_awarded: data.DebtSum + data.DebtFines + data.DebtFee,
        debt_period_start_awarded: data.FirstMonthDebt,
        debt_period_end_awarded: data.LastMonthDebt,
    }

    await pgClient.query(`
        UPDATE courts_cases SET 
            debt_charges_awarded = $1,
            debt_fines_awarded = $2,
            court_fee_awarded = $3,
            total_awarded = $4,
            debt_period_start_awarded = $5,
            debt_period_end_awarded = $6
        WHERE id = $7
    `, [...Object.values(updateData), id]);
};