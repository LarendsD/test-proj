import crypto from 'crypto';

export const encrypt = async (data: string) => {
  const hash = crypto.createHash('sha1').setEncoding('hex');
  const decrypted = hash.update(data);

  return decrypted;
};
